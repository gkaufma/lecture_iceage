# Lecture Ice-age dynamics
  
This course material is used alongside the Ice-age dynamics lecture
from the M.Sc. Geological Sciences at the Freie Universität Berlin.


## Getting Started

## Contents

- data     - data files as examples
- \*.ipynb - jupuyter notebook

## Download
```
git clone https://github.com/georgkaufmann/iceage.git
```

## Run interactively with binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/georgkaufmann/iceage.git/master?filepath=index.ipynb)

## Versioning

We use [Git](https://git-scm.com/) for versioning.

## Authors

* **Georg Kaufmann** - *Initial work* - [Georg Kaufmann](http://userpage.fu-berlin.de/~geodyn)

![](fu-logo.jpg){height=1cm}


## License

This project is licensed for classroom use only.

## Acknowledgments
